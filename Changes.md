# CraftPresence Changes

## v0.1 (?/??/2020)

### Changes

*   Initial Release (Based of CraftPresence for Minecraft v1.6.6)

### More Information

#### Converting from Minecraft to Terraria Info

Currently, Configurations will not automatically convert to Terraria's Logic, but can be transferred manually in some cases

See the Mod Description // README for More Info
