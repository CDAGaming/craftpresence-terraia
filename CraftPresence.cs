/*
 * MIT License
 *
 * Copyright (c) 2018 - 2020 CDAGaming (cstack2011@yahoo.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using CraftPresence.Utils;
using CraftPresence.Utils.Discord;
using Terraria;
using Terraria.ModLoader;

namespace CraftPresence
{
    /**
	 * The Primary Application Class and Utilities
	 *
	 * @author CDAGaming
	 */
    public class CraftPresence : Mod
    {
        /**
		 * If the Mod is Currently Closing and Clearing Data
		 */
        public static bool Closing = false;

        /**
		 * The Game Instance attached to this mod
		 */
        public static Main Instance = Main.instance;

        /**
		 * The Current Player detected from the Game Instance
		 */
        public static Player Player = Main.player[Main.myPlayer];

        /**
         * The {@link SystemUtils} Instance for this Mod
         */
        public static readonly SystemUtils System = new SystemUtils();

        /**
         * The {@link DiscordUtils} Instance for this Mod
         */
        public static readonly DiscordUtils Client = new DiscordUtils();

        /**
		 * Whether the Mod has completed it's Initialization Phase
		 */
        private bool _initialized;

        /**
		 * Begins Scheduling Ticks from Initial Load
		 */
        public override void Load()
        {
            // Setup Tick Function
            Main.OnTick += ClientTick;
        }

        public override void Unload()
        {
            // TODO - Add Shutdown method call here
            Client.ShutDown();

            _initialized = false;
            Main.OnTick -= ClientTick;
        }

        /**
		 * The Mod's Initialization Event
		 * 
		 * Comprises of Data Initialization and RPC Setup
		 */
        private void Init()
        {
            // If running in Developer Mode, Warn of Possible Issues and Log OS Info - TODO
            ModUtils.Log.DebugWarn(ModUtils.Translator.Translate(true, "craftpresence.logger.warning.debugmode"));
            ModUtils.Log.DebugInfo(ModUtils.Translator.Translate(true, "craftpresence.logger.info.os", System.OsName,
                System.OsArch, System.Is64Bit));

            CommandUtils.Init();

            // Synchronize Developer Mode - TODO
            ModUtils.IsDev = true;

            try
            {
                Client.ClientId = ModContent.GetInstance<GeneralConfig>().ClientId;
                Client.Init();
                Client.UpdateTimestamp();
            }
            catch (Exception ex)
            {
                ModUtils.Log.Error(ModUtils.Translator.Translate("craftpresence.logger.error.load"));
                ModUtils.Log.DebugError(ex.Message);
            }
            finally
            {
                _initialized = true;
            }
        }

        /**
		 * The Event to Run on each Client Tick, if passed initialization events
		 *
		 * Comprises of Synchronizing Data, and Updating RPC Data as needed
		 */
        private void ClientTick()
        {
            if (_initialized)
            {
                Instance = Main.instance;
                Player = Main.player[Main.myPlayer];

                CommandUtils.ReloadData();

                // TODO - Complete this part of the function

                if (!System.HasLoaded)
                    // Ensure Loading Presence has already passed, before any other type  of presence displays
                    CommandUtils.SetLoadingPresence();
                else if (!CommandUtils.IsInMainMenu && true) CommandUtils.SetMainMenuPresence();
                // TODO Chunk

                if (Client.AwaitingReply && System.Timer == 0)
                {
                    StringUtils.SendMessageToPlayer(Player,
                        ModUtils.Translator.Translate("craftpresence.command.request.ignored",
                            Client.RequesterUser.Username));
                    //Client.IpcInstance.Respond(CLIENT.REQUESTER_USER, false);
                    Client.AwaitingReply = false;
                    Client.Status = "ready";
                }
                else if (!Client.AwaitingReply && Client.RequesterUser != null)
                {
                    Client.RequesterUser = null;
                    Client.Status = "ready";
                }
            }
            else if (!Closing)
            {
                if (ModUtils.Translator.Initialized)
                    Init();
                else
                    ModUtils.Translator.OnTick();
            }
        }
    }
}