/*
 * MIT License
 *
 * Copyright (c) 2018 - 2020 CDAGaming (cstack2011@yahoo.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using DiscordRPC.Logging;
using log4net;

namespace CraftPresence
{
    /**
     * Logging Manager for either Sending Info to Chat or in Logs
     *
     * @author CDAGaming
     */
    public class ModLogger : ILogger
    {
        /**
         * Name of the Logger, primarily used for Chat Formatting
         */
        private readonly string LoggerName;

        /**
         * The Instance of the Root Logging Manager, for sending messages to logs
         */
        private readonly ILog LogInstance;

        public ModLogger(string newLoggerName)
        {
            LoggerName = newLoggerName;
            LogInstance = LogManager.GetLogger(LoggerName);
        }

        public void Warning(string message, params object[] args)
        {
            Warn(message, args);
        }

        /**
         * Sends a Message with an ERROR Level to either Chat or Logs
         *
         * @param logMessage   The Log Message to Send
         * @param logArguments Additional Formatting Arguments
         */
        public void Error(string logMessage, params object[] logArguments)
        {
            // TODO: Add SendToPlayer Logic here when ready.
            LogInstance.ErrorFormat(logMessage, logArguments);
        }

        public LogLevel Level { get; set; }

        public void Trace(string message, params object[] args)
        {
            DebugInfo(message, args);
        }

        /**
         * Sends a Message with an INFO Level to either Chat or Logs
         *
         * @param logMessage   The Log Message to Send
         * @param logArguments Additional Formatting Arguments
         */
        public void Info(string logMessage, params object[] logArguments)
        {
            // TODO: Add SendToPlayer Logic here when ready.
            LogInstance.InfoFormat(logMessage, logArguments);
        }

        /**
         * Sends a Message with an WARNING Level to either Chat or Logs
         *
         * @param logMessage   The Log Message to Send
         * @param logArguments Additional Formatting Arguments
         */
        public void Warn(string logMessage, params object[] logArguments)
        {
            // TODO: Add SendToPlayer Logic here when ready.
            LogInstance.WarnFormat(logMessage, logArguments);
        }

        /**
         * Sends a Message with an INFO Level to either Chat or Logs, if in Debug Mode
         *
         * @param logMessage   The Log Message to Send
         * @param logArguments Additional Formatting Arguments
         */
        public void DebugInfo(string logMessage, params object[] logArguments)
        {
            if (ModUtils.IsDev) Info("[Debug] " + logMessage, logArguments);
        }

        /**
         * Sends a Message with an WARNING Level to either Chat or Logs, if in Debug Mode
         *
         * @param logMessage   The Log Message to Send
         * @param logArguments Additional Formatting Arguments
         */
        public void DebugWarn(string logMessage, params object[] logArguments)
        {
            if (ModUtils.IsDev) Warn("[Debug] " + logMessage, logArguments);
        }

        /**
         * Sends a Message with an ERROR Level to either Chat or Logs, if in Debug Mode
         *
         * @param logMessage   The Log Message to Send
         * @param logArguments Additional Formatting Arguments
         */
        public void DebugError(string logMessage, params object[] logArguments)
        {
            if (ModUtils.IsDev) Error("[Debug] " + logMessage, logArguments);
        }
    }
}