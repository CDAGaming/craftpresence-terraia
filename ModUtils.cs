/*
 * MIT License
 *
 * Copyright (c) 2018 - 2020 CDAGaming (cstack2011@yahoo.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using CraftPresence.Utils;
using Steamworks;
using Terraria.ModLoader;

namespace CraftPresence
{
    /**
     * Constant Variables and Methods used throughout the Application
     *
     * @author CDAGaming
     */
    public static class ModUtils
    {
        /**
         * The Application's Name
         */
        public const string Name = "CraftPresence";

        /**
         * The Application's Major Revision Number (Ex: 1 in 1.0.2)
         */
        public const string MajorVersion = "1";

        /**
         * The Application's Minor Revision Number (Ex: 0 in 1.0.2)
         */
        public const string MinorVersion = "6";

        /**
         * The Application's Revision Version Number (Ex: 2 in 1.0.2)
         */
        public const string RevisionVersion = "5";

        /**
         * The Application's Formatted Version ID
         */
        public static readonly string VersionId = "v" + MajorVersion + "." + MinorVersion + "." + RevisionVersion;

        /**
         * The Application's Identifier (Same as Name in the Case of Terraria)
         */
        public static readonly string ModId = Name;

        /**
         * The Detected Game Version
         */
        public static readonly string GameVersion = FrameworkVersion.Version.ToString();

        /**
         * The Detected Framework Brand Information
         */
        public static readonly string Brand = FrameworkVersion.Framework.ToString();

        /**
         * The Detected Username within the Game
         */
        public static readonly string Username = SteamFriends.GetPersonaName();

        /**
         * The URL to receive Update Information from
         */
        public static readonly string UpdateJson =
            "https://raw.githubusercontent.com/CDAGaming/VersionLibrary/master/CraftPresence/update.json";

        /**
         * The Application's Instance of {@link ModLogger} for Logger Information
         */
        public static readonly ModLogger Log = new ModLogger(ModId);

        /**
         * The application's Instance of {@link TranslationUtils} for Localization and Translating Data Strings
         */
        public static readonly TranslationUtils Translator = new TranslationUtils(ModId);

        /**
         * If this Application is running/needs Legacy Data
         */
        public static readonly bool IsLegacy = false;

        /**
         * Whether to forcibly block any tooltips related to this application from rendering
         */
        public static bool ForceBlockTooltipRendering = false;

        /**
         * If this Application should be run in a Developer or Debug State
         */
        public static bool IsDev = true;
    }
}