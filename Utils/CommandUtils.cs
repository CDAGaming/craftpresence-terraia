/*
 * MIT License
 *
 * Copyright (c) 2018 - 2020 CDAGaming (cstack2011@yahoo.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using CraftPresence.Utils.Discord.Assets;
using Terraria.ModLoader;

namespace CraftPresence.Utils
{
    /**
     * Command Utilities for Synchronizing and Initializing Data
     *
     * @author CDAGaming
     */
    public static class CommandUtils
    {
        /**
         * Whether you are on the Main Menu in Terraria
         */
        public static bool IsInMainMenu;

        /**
         * Whether you are in a Loading State
         */
        public static bool IsLoadingGame;

        /**
         * Reloads and Synchronizes Data, as needed, and performs onTick Events
         *
         * @param forceUpdateRpc Whether to Force an Update to the RPC Data
         */
        public static void ReloadData(bool forceUpdateRpc = false)
        {
            ModUtils.Translator.OnTick();
            CraftPresence.System.OnTick();

            // TODO - Under Construction

            if (!IsInMainMenu)
                // TODO

                if (forceUpdateRpc)
                {
                    // TODO
                }
        }

        /**
         * Restarts and Initializes the Rpc Data
         */
        public static void RebootRpc()
        {
            CraftPresence.Client.ShutDown();
            CraftPresence.System.HasLoaded = false;

            if (!CraftPresence.Client.ClientId.Equals(ModContent.GetInstance<GeneralConfig>().ClientId))
            {
                DiscordAssetUtils.EmptyData();
                CraftPresence.Client.ClientId = ModContent.GetInstance<GeneralConfig>().ClientId;
            }
            else
            {
                DiscordAssetUtils.ClearClientData();
            }

            DiscordAssetUtils.LoadAssets();
            CraftPresence.Client.Init();
        }

        /**
         * Initializes Essential Data
         * (In this case, the Available Rpc Icons)
         */
        public static void Init()
        {
            DiscordAssetUtils.LoadAssets();
        }

        /**
         * Synchronizes RPC Data towards that of being in a Loading State
         */
        public static void SetLoadingPresence()
        {
            // Form Argument Lists
            var loadingArgs = new List<Tuple<string, string>>();

            // Add All Generalized Arguments, if any
            if (CraftPresence.Client.GeneralArgs.Any()) loadingArgs.AddRange(CraftPresence.Client.GeneralArgs);

            CraftPresence.Client.Status = "ready";
            CraftPresence.Client.ClearPartyData(true, false);

            CraftPresence.Client.SyncArgument("&MAINMENU&",
                StringUtils.SequentialReplaceAnyCase(ModContent.GetInstance<StatusMessagesConfig>().LoadingMsg,
                    loadingArgs));
            CraftPresence.Client.SyncArgument("&MAINMENU&",
                CraftPresence.Client.ImageOf(ModContent.GetInstance<GeneralConfig>().DefaultIcon, "", false), true);

            IsLoadingGame = true;
        }

        /**
         * Synchronizes Rpc Data towards that of being in the Main Menu
         */
        public static void SetMainMenuPresence()
        {
            // Form Argument Lists
            var mainMenuArgs = new List<Tuple<string, string>>();

            // Add All Generalized Arguments, if any
            if (CraftPresence.Client.GeneralArgs.Any()) mainMenuArgs.AddRange(CraftPresence.Client.GeneralArgs);

            // Clear Loading Game State, if applicable
            if (IsLoadingGame)
            {
                CraftPresence.Client.InitArgumentData(false, "&MAINMENU&");
                CraftPresence.Client.InitArgumentData(true, "&MAINMENU&");

                IsLoadingGame = false;
            }

            CraftPresence.Client.SyncArgument("&MAINMENU&",
                StringUtils.SequentialReplaceAnyCase(ModContent.GetInstance<StatusMessagesConfig>().MainMenuMsg,
                    mainMenuArgs));
            CraftPresence.Client.SyncArgument("&MAINMENU&",
                CraftPresence.Client.ImageOf(ModContent.GetInstance<GeneralConfig>().DefaultIcon, "", false), true);

            IsInMainMenu = true;
        }
    }
}