/*
 * MIT License
 *
 * Copyright (c) 2018 - 2020 CDAGaming (cstack2011@yahoo.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System.ComponentModel;
using System.Runtime.Serialization;
using Terraria.ModLoader.Config;

//
// Configuration Data for this Mod
// TERRARIA EXCLUSIVE CLASS - DO NOT MERGE WITH MC LOGIC!
//
namespace CraftPresence.Utils
{
    [Label("General Settings")]
    public class GeneralConfig : ModConfig
    {
        [Label("Client ID")]
        [Tooltip("" +
                 "Client ID Used for retrieving Assets, Icon Keys, and titles" +
                 "\n" +
                 "Must be an 18-digit number to be valid"
            )
        ]
        [DefaultValue("712703889320706078")]
        public string ClientId = "712703889320706078";

        [Label("Default Icon")] [DefaultValue("terraria")]
        public string DefaultIcon = "terraria";

        [Label("Show Time")] [DefaultValue(true)]
        public bool ShowTime = true;

        public override ConfigScope Mode => ConfigScope.ClientSide;

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            // Add any Version-Upgrade Data Here
        }
    }

    [Label("Status Messages")]
    public class StatusMessagesConfig : ModConfig
    {
        [Label("Loading Message")] [DefaultValue("Loading...")]
        public string LoadingMsg = "Loading...";

        [Label("Main Menu Message")] [DefaultValue("In the Main Menu")]
        public string MainMenuMsg = "In the Main Menu";

        public override ConfigScope Mode => ConfigScope.ClientSide;

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            // Add any Version-Upgrade Data Here
        }
    }

    [Label("Display Messages")]
    public class DisplayMessagesConfig : ModConfig
    {
        [Label("Details Message")] [DefaultValue("&MAINMENU&")]
        public string DetailsMsg = "&MAINMENU&";

        [Label("Game State Message")] [DefaultValue("&SERVER&")]
        public string GameStateMsg = "&SERVER&";

        [Label("Large Image Key")] [DefaultValue("&MAINMENU&")]
        public string LargeImageKey = "&MAINMENU&";

        [Label("Large Image Message")] [DefaultValue("&MAINMENU&")]
        public string LargeImageMsg = "&MAINMENU&";

        [Label("Small Image Key")] [DefaultValue("&SERVER&")]
        public string SmallImageKey = "&SERVER&";

        [Label("Small Image Message")] [DefaultValue("&SERVER&")]
        public string SmallImageMsg = "&SERVER&";

        public override ConfigScope Mode => ConfigScope.ClientSide;

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            // Add any Version-Upgrade Data Here
        }
    }

    [Label("Advanced Settings")]
    public class AdvancedSettingsConfig : ModConfig
    {
        [Label("Format Words")] [DefaultValue(true)]
        public bool FormatWords = true;

        [Label("Refresh Rate")] [DefaultValue(2)]
        public int RefreshRate = 2;

        public override ConfigScope Mode => ConfigScope.ClientSide;

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            // Add any Version-Upgrade Data Here
        }
    }

    [Label("Accessibility Settings")]
    public class AccessibilitySettingsConfig : ModConfig
    {
        [Label("Language Id")] [DefaultValue("en_us")]
        public string LanguageId = "en_us";

        [Label("Strip Translation Colors")] [DefaultValue(false)]
        public bool StripTranslationColors = false;

        public override ConfigScope Mode => ConfigScope.ClientSide;

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            // Add any Version-Upgrade Data Here
        }
    }
}