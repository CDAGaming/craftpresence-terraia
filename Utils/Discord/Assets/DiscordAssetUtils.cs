/*
 * MIT License
 *
 * Copyright (c) 2018 - 2020 CDAGaming (cstack2011@yahoo.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using Terraria.ModLoader;

namespace CraftPresence.Utils.Discord.Assets
{
    /**
     * Utilities related to locating and Parsing available Discord Assets
     * Uses the current Client Id in use to locate Discord Icons and related Assets
     *
     * @author CDAGaming
     */
    public static class DiscordAssetUtils
    {
        /**
         * A List of the Icon IDs available as ImageType SMALL
         */
        private static readonly List<string> SmallIds = new List<string>();

        /**
         * A List of the Icon IDs available as ImageType LARGE
         */
        private static readonly List<string> LargeIds = new List<string>();

        /**
         * A List of all the Icon IDs available within the Current Client ID
         */
        private static readonly List<string> IconIds = new List<string>();

        /**
         * If the Asset Check had completed
         */
        public static bool SyncCompleted;

        /**
         * A List of the Icons available as ImageType SMALL
         */
        public static readonly List<string> SmallIcons = new List<string>();

        /**
         * A List of the Icons available as ImageType LARGE
         */
        public static readonly List<string> LargeIcons = new List<string>();

        /**
         * A List of all the Icons available within the Current Client ID
         */
        public static readonly List<string> IconList = new List<string>();

        /**
         * A List of all the Icon IDs available within the Current Client ID
         */
        private static Dictionary<string, DiscordAsset> _assetList = new Dictionary<string, DiscordAsset>();

        /**
         * Determines if the Specified Icon Key is present under the Current Client ID
         *
         * @param key The Specified Icon Key to Check
         * @return {@code true} if the Icon Key is present and able to be used
         */
        public static bool Contains(string key)
        {
            var formattedKey =
                StringUtils.IsNullOrEmpty(key) ? "" : StringUtils.FormatPackIcon(key.Replace(' ', '_'));
            return _assetList.ContainsKey(formattedKey);
        }

        /**
         * Retrieves the Specified {@link DiscordAsset} data from an Icon Key, if present
         *
         * @param key The Specified Icon Key to gain info for
         * @return The {@link DiscordAsset} data for this Icon Key
         */
        public static DiscordAsset Get(string key)
        {
            var formattedKey =
                StringUtils.IsNullOrEmpty(key) ? "" : StringUtils.FormatPackIcon(key.Replace(' ', '_'));
            return Contains(formattedKey) ? _assetList[formattedKey] : null;
        }

        /**
         * Retrieves the Parsed Icon Key from the specified key, if present
         *
         * @param key The Specified Key to gain info for
         * @return The Parsed Icon Key from the {@link DiscordAsset} data
         */
        public static string GetKey(string key)
        {
            return Get(key) != null ? Get(key).GetName() : "";
        }

        /**
         * Retrieves the Parsed Icon ID from the specified key, if present
         *
         * @param key The Specified Key to gain info for
         * @return The Parsed Icon ID from the {@link DiscordAsset} data
         */
        public static string GetId(string key)
        {
            return Get(key) != null ? Get(key).GetId() : "";
        }

        /**
         * Retrieves the Parsed Image Type from the specified key, if present
         *
         * @param key The Specified Key to gain info for
         * @return The Parsed Image Type from the {@link DiscordAsset} data
         */
        public static DiscordAsset.AssetType GetAssetType(string key)
        {
            return Get(key) != null ? Get(key).GetAssetType() : DiscordAsset.AssetType.Large;
        }

        /**
         * Clears FULL Data from this Module
         */
        public static void EmptyData()
        {
            _assetList.Clear();
            SmallIcons.Clear();
            SmallIds.Clear();
            LargeIcons.Clear();
            LargeIds.Clear();
            IconList.Clear();
            IconIds.Clear();

            ClearClientData();
        }

        /**
         * Clears Runtime Client Data from this Module (PARTIAL Clear)
         */
        public static void ClearClientData()
        {
            SyncCompleted = false;
        }

        /**
         * Attempts to retrieve a Random Icon Key from the available assets
         *
         * @return A Randomly retrieved Icon Key, if found
         */
        public static string GetRandomAsset()
        {
            try
            {
                var randomObj = new Random();
                return IconList[randomObj.Next(IconList.Count)];
            }
            catch (Exception ex)
            {
                ModUtils.Log.Error(
                    ModUtils.Translator.Translate("craftpresence.logger.error.config.invalidicon.empty"));
                ModUtils.Log.DebugError(ex.Message);
                return "";
            }
        }

        /**
         * Retrieves and Synchronizes the List of Available Discord Assets from the Client ID
         */
        public static void LoadAssets()
        {
            ModUtils.Log.Info(ModUtils.Translator.Translate("craftpresence.logger.info.discord.assets.load",
                ModContent.GetInstance<GeneralConfig>().ClientId));
            ModUtils.Log.Info(ModUtils.Translator.Translate("craftpresence.logger.info.discord.assets.load.credits"));
            _assetList = new Dictionary<string, DiscordAsset>();

            try
            {
                var url = "https://discordapp.com/api/oauth2/applications/" +
                          ModContent.GetInstance<GeneralConfig>().ClientId + "/assets";
                // TODO: Migrate below line partially to FileUtils during reconstruction
                var assets = JsonConvert.DeserializeObject<DiscordAsset[]>(GetWebContent(url));

                if (assets != null)
                    foreach (var asset in assets)
                    {
                        if (asset.GetAssetType().Equals(DiscordAsset.AssetType.Large))
                        {
                            if (!LargeIcons.Contains(asset.GetName())) LargeIcons.Add(asset.GetName());

                            if (!LargeIds.Contains(asset.GetId())) LargeIds.Add(asset.GetId());
                        }

                        if (asset.GetAssetType().Equals(DiscordAsset.AssetType.Small))
                        {
                            if (!SmallIcons.Contains(asset.GetName())) SmallIcons.Add(asset.GetName());

                            if (!SmallIds.Contains(asset.GetId())) SmallIds.Add(asset.GetId());
                        }

                        if (!IconList.Contains(asset.GetName())) IconList.Add(asset.GetName());

                        if (!_assetList.ContainsKey(asset.GetName())) _assetList.Add(asset.GetName(), asset);

                        if (!IconIds.Contains(asset.GetId())) IconIds.Add(asset.GetId());
                    }
            }
            catch (Exception ex)
            {
                ModUtils.Log.Error(ModUtils.Translator.Translate("craftpresence.logger.error.discord.assets.load"));
                ModUtils.Log.DebugError(ex.Message);
            }
            finally
            {
                VerifyConfigAssets();
                SyncCompleted = true;
                ModUtils.Log.Info(ModUtils.Translator.Translate("craftpresence.logger.info.discord.assets.detected",
                    _assetList.Count));
            }
        }

        /**
         * Ensures any Default Icons in the config exist within the Client ID
         */
        private static void VerifyConfigAssets()
        {
            // TODO - Potentially Remove as unneeded since using basic Screens
        }

        // TODO: Move when FileUtils is Reconstructed
        private static string GetWebContent(string url)
        {
            var uri = new Uri(url);
            var request = (HttpWebRequest) WebRequest.Create(uri);
            request.Method = WebRequestMethods.Http.Get;
            var response = (HttpWebResponse) request.GetResponse();
            var dataStream = response.GetResponseStream();

            if (dataStream == null) return "";
            var reader = new StreamReader(dataStream);
            var output = reader.ReadToEnd();
            response.Close();

            return output;
        }
    }
}