/*
 * MIT License
 *
 * Copyright (c) 2018 - 2020 CDAGaming (cstack2011@yahoo.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using CraftPresence.Utils.Discord.Assets;
using DiscordRPC;
using DiscordRPC.Logging;
using Terraria.ModLoader;

namespace CraftPresence.Utils.Discord
{
    /**
     * Variables and Methods used to update the RPC Presence States to display within Discord
     *
     * @author CDAGaming
     */
    public class DiscordUtils
    {
        /**
         * A Mapping of the Arguments available to use as Icon Key Placeholders
         */
        private readonly List<Tuple<string, string>> _iconData = new List<Tuple<string, string>>();

        /**
         * A Mapping of the Arguments available to use as RPC Message Placeholders
         */
        private readonly List<Tuple<string, string>> _messageData = new List<Tuple<string, string>>();

        /**
         * A Mapping of the Arguments attached to the &MODS& RPC Message placeholder
         */
        private readonly List<Tuple<string, string>> _modsArgs = new List<Tuple<string, string>>();

        /**
         * A Mapping of the Arguments attached o the &IGN& RPC Message Placeholder
         */
        private readonly List<Tuple<string, string>> _playerInfoArgs = new List<Tuple<string, string>>();

        /**
         * An Instance containing the Current Rich Presence Data
         * Also used to prevent sending duplicate packets with the same presence data, if any
         */
        private RichPresence _currentPresence;

        /**
         * A Mapping of the Last requested Image Data
         * Used to Prevent sending duplicate packets and cache data for repeated images in other areas
         * Format: lastAttemptedKey, lastResultingKey
         */
        private Tuple<string, string> _lastRequestedImageData = new Tuple<string, string>("", "");

        /**
         * Whether Discord is currently awaiting a response to a Ask to Join or Spectate Request, if any
         */
        public bool AwaitingReply;

        /**
         * The 18-character Client Id Number, tied to the game profile data attacked to the RPC
         */
        public string ClientId;

        /**
         * The Current User, tied to the Rich Presence
         */
        public User CurrentUser;

        /**
         * The Current Message tied to the current action / Details Field of the RPC
         */
        public string Details;

        /**
         * The Current Ending Unix Timestamp from Epoch, used for Time Until if combined with {@link DiscordUtils#StartTimestamp}
         */
        public DateTime EndTimestamp;

        /**
         * The Current Message tied to the Party/Game status Field of the RPC
         */
        public string GameState;

        /**
         * A Mapping of the General RPC Arguments, allowed in adjusting Presence Messages
         */
        public List<Tuple<string, string>> GeneralArgs = new List<Tuple<string, string>>();

        /**
         * The Instance Code attached to the RPC, if any
         */
        public byte Instance;

        /**
         * An Instance of the {@link aab}, responsible for sending and receiving RPC Events
         */
        public DiscordRpcClient IpcInstance;

        /**
         * The Current Party Join Secret Key, if in a Party
         */
        public string JoinSecret;

        /**
         * The Current Large Image Icon being displayed in the RPC, if any
         */
        public string LargeImageKey;

        /**
         * The Current Message tied to the Large Image, if any
         */
        public string LargeImageText;

        /**
         * The Current Match Secret Key tied to the RPC, if any
         */
        public string MatchSecret;

        /**
         * The Party Session Id that's tied to the RPC, if any
         */
        public string PartyId;

        /**
         * The Maximum Size of the Party Session, if in a Party
         */
        public int PartyMax;

        /**
         * The Current Size of the Party Session, if in a Party
         */
        public int PartySize;

        /**
         * The Join Request User Data, if any
         */
        public User RequesterUser;

        /**
         * The Current Small Image Icon being displayed in the RPC, if any
         */
        public string SmallImageKey;

        /**
         * The Current Message tied to the Small Image, if any
         */
        public string SmallImageText;

        /**
         * The Current Spectate Secret Key tied to the RPC, if any
         */
        public string SpectateSecret;

        /**
         * The Current Starting Unix Timestamp from Epoch, used for Elapsed Time
         */
        public DateTime StartTimestamp;

        /**
         * The current RPC Status (Ex: ready, errored, disconnected)
         */
        public string Status;

        /**
         * Initializes and Synchronizes Initial Rich Presence Data
         */
        public void Init()
        {
            try
            {
                // Create IPC Instance and Make a Connection is possible
                IpcInstance = new DiscordRpcClient(ModContent.GetInstance<GeneralConfig>().ClientId, -1,
                    new ModLogger("CraftPresence-RPC") {Level = LogLevel.Trace}, false);
                IpcInstance.RegisterUriScheme("1281930");

                // Subscribe to Listener Events - Copy Pasta of ModIPCListener from Minecraft Edition
                IpcInstance.OnReady += (sender, msg) =>
                {
                    if (!StringUtils.IsNullOrEmpty(Status) &&
                        (StringUtils.IsNullOrEmpty(Status) ||
                         Status.Equals("ready", StringComparison.OrdinalIgnoreCase))) return;
                    Status = "ready";
                    CurrentUser = IpcInstance.CurrentUser;
                    ModUtils.Log.Info(ModUtils.Translator.Translate("craftpresence.logger.info.load", ClientId,
                        CurrentUser != null ? CurrentUser.Username : "null"));
                };
                IpcInstance.OnRpcMessage += (sender, msg) =>
                {
                    // N/A -> Equivalent to onPacketSent
                };
                IpcInstance.OnPresenceUpdate += (sender, args) =>
                {
                    // N/A -> Equivalent to onPacketReceived
                };
                IpcInstance.OnError += (sender, args) =>
                {
                    if (!StringUtils.IsNullOrEmpty(Status) &&
                        (StringUtils.IsNullOrEmpty(Status) ||
                         Status.Equals("disconnected", StringComparison.OrdinalIgnoreCase))) return;
                    Status = "disconnected";
                    ModUtils.Log.Error(ModUtils.Translator.Translate("craftpresence.logger.error.rpc", args.Message));
                    ShutDown();
                };
                IpcInstance.OnClose += (sender, args) =>
                {
                    // N/A
                };
                IpcInstance.OnJoinRequested += (sender, args) =>
                {
                    // On Receiving a New Join Request - TODO Add Ability to Accept this again
                    if (!StringUtils.IsNullOrEmpty(Status) &&
                        (StringUtils.IsNullOrEmpty(Status) ||
                         Status.Equals("joinRequest", StringComparison.OrdinalIgnoreCase) &&
                         RequesterUser.Equals(args.User))) return;
                    CraftPresence.System.Timer = 30;
                    Status = "joinRequest";
                    RequesterUser = args.User;
                };
                IpcInstance.OnSpectate += (sender, args) =>
                {
                    // Spectating Game, Unimplemented for now
                    if (StringUtils.IsNullOrEmpty(Status) || !StringUtils.IsNullOrEmpty(Status) &&
                        !Status.Equals("spectateGame",
                            StringComparison.OrdinalIgnoreCase))
                        Status = "spectateGame";
                };
                IpcInstance.OnJoin += (sender, args) =>
                {
                    // On Accepting and Queuing a Join Request - TODO Do when Server Logic is Re-Implemented
                    if (!StringUtils.IsNullOrEmpty(Status) &&
                        (StringUtils.IsNullOrEmpty(Status) ||
                         Status.Equals("joinGame", StringComparison.OrdinalIgnoreCase))) return;
                    Status = "joinGame";
                    //CraftPresence.SERVER.verifyAndJoin(args.Secret);
                };

                //IpcInstance.SetSubscription(EventType.Join | EventType.Spectate | EventType.JoinRequest);
            }
            catch (Exception ex)
            {
                ModUtils.Log.DebugError(ex.Message);
            }

            // Initialize and Sync any Pre-made Arguments (And Reset Related Data)
            InitArgumentData(false, "&MAINMENU&", "&BRAND&", "&MCVERSION&", "&IGN&", "&MODS&", "&PACK&", "&DIMENSION&",
                "&BIOME&", "&SERVER&", "&GUI&", "&TILEENTITY&", "&TARGETENTITY&", "&ATTACKINGENTITY&",
                "&RIDINGENTITY&");
            InitArgumentData(true, "&MAINMENU&", "&BRAND&", "&MCVERSION&", "&IGN&", "&MODS&", "&PACK&", "&DIMENSION&",
                "&BIOME&", "&SERVER&", "&GUI&", "&TILEENTITY&", "&TARGETENTITY&", "&ATTACKINGENTITY&",
                "&RIDINGENTITY&");

            // Ensure Main Menu RPC Resets properly
            CommandUtils.IsInMainMenu = false;

            // Add Any Generalized Argument Data needed - TODO

            foreach (var generalArgument in GeneralArgs)
                // For each General (Can be used anywhere) Argument
                // Ensure they sync as Formatter Arguments too
                SyncArgument(generalArgument.Item1, generalArgument.Item2);

            // Sync the Default Icon Argument
            SyncArgument("&DEFAULT&", ModContent.GetInstance<GeneralConfig>().DefaultIcon, true);

            IpcInstance.Initialize();
        }

        /**
         * Updates the Starting Unix Timestamp, if allowed
         */
        public void UpdateTimestamp()
        {
            if (ModContent.GetInstance<GeneralConfig>().ShowTime) StartTimestamp = CraftPresence.System.CurrentDateTime;
        }

        /**
         * Synchronizes the Specified Argument as an RPC Message or an Icon Placeholder
         *
         * @param argumentName The Specified Argument to Synchronize for
         * @param insertString The String to attach to the Specified Argument
         * @param isIconData   Whether the Argument is an RPC Message or an Icon Placeholder
         */
        public void SyncArgument(string argumentName, string insertString, bool isIconData = false)
        {
            // Remove and Replace Placeholder Data, if the placeholder needs Updates
            if (StringUtils.IsNullOrEmpty(argumentName)) return;
            if (isIconData)
            {
                _iconData.RemoveAll(e =>
                    e.Item1.Equals(argumentName, StringComparison.OrdinalIgnoreCase) &&
                    (StringUtils.IsNullOrEmpty(insertString) ||
                     !e.Item2.Equals(insertString, StringComparison.OrdinalIgnoreCase)));
                _iconData.Add(new Tuple<string, string>(argumentName, insertString));
            }
            else
            {
                _messageData.RemoveAll(e =>
                    e.Item1.Equals(argumentName, StringComparison.OrdinalIgnoreCase) &&
                    (StringUtils.IsNullOrEmpty(insertString) ||
                     !e.Item2.Equals(insertString, StringComparison.OrdinalIgnoreCase)));
                _messageData.Add(new Tuple<string, string>(argumentName, insertString));
            }
        }

        /**
         * Initialize the Specified RPC Arguments as Empty Data
         *
         * @param isIcon Whether the Argument to initialize is an Icon Argument
         * @param args The Arguments to Initialize
         */
        public void InitArgumentData(bool isIcon, params string[] args)
        {
            // Initialize Specified Arguments to Empty Data
            foreach (var argumentName in args) SyncArgument(argumentName, "", isIcon);
        }

        /**
         * Synchronizes and Updates the Rich Presence Data, if needed and connected
         *
         * @param presence The New Presence Data to apply
         */
        public void UpdatePresence(RichPresence presence)
        {
            if (presence == null || _currentPresence != null && presence.Equals(_currentPresence) ||
                !IpcInstance.IsInitialized) return;
            IpcInstance.SetPresence(presence);
            _currentPresence = presence;
        }

        /**
         * Attempts to lookup the specified Image, and if not existent, use the alternative String, and null if allowed
         *
         * @param evalString        The Specified Icon Key to search for from the {@link DiscordUtils#CLIENT_ID} Assets
         * @param alternativeString The Alternative Icon Key to use if unable to locate the Original Icon Key
         * @param allowNull         If allowed to return null if unable to find any matches, otherwise uses the Default Icon in Config
         * @return The found or alternative matching Icon Key
         */
        public string ImageOf(string evalString, string alternativeString, bool allowNull)
        {
            // Ensures Assets are fully synced from the Client Id before running
            if (!DiscordAssetUtils.SyncCompleted) return "";
            if (!StringUtils.IsNullOrEmpty(_lastRequestedImageData.Item1) &&
                _lastRequestedImageData.Item1.Equals(evalString, StringComparison.OrdinalIgnoreCase))
                return _lastRequestedImageData.Item2;
            var defaultIcon = DiscordAssetUtils.Contains(ModContent.GetInstance<GeneralConfig>().DefaultIcon)
                ? ModContent.GetInstance<GeneralConfig>().DefaultIcon
                : DiscordAssetUtils.GetRandomAsset();
            _lastRequestedImageData = new Tuple<string, string>(evalString, _lastRequestedImageData.Item2);

            var finalKey = evalString;

            if (!DiscordAssetUtils.Contains(finalKey))
            {
                ModUtils.Log.Error(ModUtils.Translator.Translate(true,
                    "craftpresence.logger.error.discord.assets.fallback", evalString, alternativeString));
                ModUtils.Log.Info(ModUtils.Translator.Translate(true,
                    "craftpresence.logger.info.discord.assets.request", evalString));
                if (DiscordAssetUtils.Contains(alternativeString))
                {
                    ModUtils.Log.Info(ModUtils.Translator.Translate(true,
                        "craftpresence.logger.info.discord.assets.fallback", evalString, alternativeString));
                    finalKey = alternativeString;
                }
                else
                {
                    if (allowNull)
                    {
                        finalKey = "";
                    }
                    else
                    {
                        ModUtils.Log.Info(ModUtils.Translator.Translate(true,
                            "craftpresence.logger.error.discord.assets.default", evalString));
                        finalKey = defaultIcon;
                    }
                }
            }

            _lastRequestedImageData = new Tuple<string, string>(_lastRequestedImageData.Item1, finalKey);
            return finalKey;
        }

        /**
         * Clears Related Party Session Information from the RPC, and updates if needed
         *
         * @param clearRequesterData Whether to clear Ask to Join / Spectate Request Data
         * @param updateRPC          Whether to immediately update the RPC following changes
         */
        public void ClearPartyData(bool clearRequesterData, bool updateRpc)
        {
            if (clearRequesterData)
            {
                AwaitingReply = false;
                RequesterUser = null;
                CraftPresence.System.Timer = 0;
            }

            JoinSecret = null;
            PartyId = null;
            PartySize = 0;
            PartyMax = 0;
            if (updateRpc) UpdatePresence(BuildRichPresence());
        }

        /**
         * Shutdown the RPC and close related resources, as well as clearing any runtime client data
         */
        public void ShutDown()
        {
            CraftPresence.Closing = true;
            try
            {
                IpcInstance.Dispose();
            }
            catch (Exception ex)
            {
                ModUtils.Log.DebugError(ex.Message);
            }

            // Clear User Data before final clear and shutdown
            Status = "disconnected";
            _currentPresence = null;
            ClearPartyData(true, false);
            CurrentUser = null;

            _lastRequestedImageData = new Tuple<string, string>("", "");

            // TODO

            ModUtils.Log.Info(ModUtils.Translator.Translate("craftpresence.logger.info.shutdown"));
        }

        /**
         * Builds a New Instance of {@link RichPresence} based on Queued Data
         *
         * @return A New Instance of {@link RichPresence}
         */
        public RichPresence BuildRichPresence()
        {
            // Format Presence based on Arguments available in argumentData
            Details = StringUtils.FormatWord(
                StringUtils.SequentialReplaceAnyCase(ModContent.GetInstance<DisplayMessagesConfig>().DetailsMsg,
                    _messageData), !ModContent.GetInstance<AdvancedSettingsConfig>().FormatWords);
            GameState = StringUtils.FormatWord(
                StringUtils.SequentialReplaceAnyCase(ModContent.GetInstance<DisplayMessagesConfig>().GameStateMsg,
                    _messageData), !ModContent.GetInstance<AdvancedSettingsConfig>().FormatWords);

            var baseLargeImage = StringUtils.RemoveMatches(
                StringUtils.GetMatches("&([^\\s]+?)&", ModContent.GetInstance<DisplayMessagesConfig>().LargeImageKey),
                _iconData, 1);
            LargeImageKey = StringUtils.SequentialReplaceAnyCase(baseLargeImage, _iconData);

            var baseSmallImage = StringUtils.RemoveMatches(
                StringUtils.GetMatches("&([^\\s]+?)&", ModContent.GetInstance<DisplayMessagesConfig>().SmallImageKey),
                _iconData, 1);
            SmallImageKey = StringUtils.SequentialReplaceAnyCase(baseSmallImage, _iconData);

            LargeImageText =
                StringUtils.FormatWord(
                    StringUtils.SequentialReplaceAnyCase(ModContent.GetInstance<DisplayMessagesConfig>().LargeImageMsg,
                        _messageData), !ModContent.GetInstance<AdvancedSettingsConfig>().FormatWords);
            SmallImageText =
                StringUtils.FormatWord(
                    StringUtils.SequentialReplaceAnyCase(ModContent.GetInstance<DisplayMessagesConfig>().SmallImageMsg,
                        _messageData), !ModContent.GetInstance<AdvancedSettingsConfig>().FormatWords);

            var newRpcData = new RichPresence
            {
                Details = Details,
                State = GameState,
                Timestamps = new Timestamps
                {
                    Start = StartTimestamp //,
                    //End = EndTimestamp
                },
                Assets = new DiscordRPC.Assets
                {
                    LargeImageKey = LargeImageKey,
                    SmallImageKey = SmallImageKey,
                    LargeImageText = LargeImageText,
                    SmallImageText = SmallImageText
                } /*,
                Party = new Party()
                {
                    ID = PartyId,
                    Max = PartyMax,
                    Size = PartySize
                },
                Secrets = new Secrets()
                {
                    JoinSecret = JoinSecret,
                    //MatchSecret = MatchSecret,
                    SpectateSecret = SpectateSecret
                }*/
            };

            // Format Data to UTF_8 after Sent to RPC (RPC may have it's own encoding)
            GameState = StringUtils.GetConvertedString(GameState, "UTF-8", false);
            Details = StringUtils.GetConvertedString(Details, "UTF-8", false);

            LargeImageKey = StringUtils.GetConvertedString(LargeImageKey, "UTF-8", false);
            SmallImageKey = StringUtils.GetConvertedString(SmallImageKey, "UTF-8", false);

            LargeImageText = StringUtils.GetConvertedString(LargeImageText, "UTF-8", false);
            SmallImageText = StringUtils.GetConvertedString(SmallImageText, "UTF-8", false);

            return newRpcData;
        }
    }
}