/*
 * MIT License
 *
 * Copyright (c) 2018 - 2020 CDAGaming (cstack2011@yahoo.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Terraria;

namespace CraftPresence.Utils
{
    /**
     * String Utilities for interpreting Strings and Basic Data Types
     *
     * @author CDAGaming
     */
    public static class StringUtils
    {
        /**
         * Converts a String and it's bytes to that of the Specified Charset
         *
         * @param original The original String
         * @param encoding The Charset to encode the String under
         * @param decode   If we are Decoding an already encoded String
         */
        public static string GetConvertedString(string original, string encoding, bool decode)
        {
            try
            {
                var conversionEncoding = Encoding.GetEncoding(encoding);
                if (decode) return conversionEncoding.GetString(original.ToByteArray()).Replace("\\s+", " ");

                var convertBytes =
                    Encoding.Convert(Encoding.Default, conversionEncoding, original.ToByteArray());
                return conversionEncoding.GetString(convertBytes);
            }
            catch (Exception)
            {
                return original;
            }
        }

        /**
         * Retrieve Matching Values from an input that matches the defined regex
         *
         * @param regexValue The Regex Value to test against
         * @param original   The original Object to get matches from
         * @return A Tuple with the Format of originalString:listOfMatches
         */
        public static Tuple<string, List<string>> GetMatches(string regexValue, object original)
        {
            return original != null
                ? GetMatches(regexValue, original.ToString())
                : new Tuple<string, List<string>>("", new List<string>());
        }

        /**
         * Retrieve Matching Values from an input that matches the defined regex
         *
         * @param regexValue The Regex Value to test against
         * @param original   The original String to get matches from
         * @return A Tuple with the Format of originalString:listOfMatches
         */
        public static Tuple<string, List<string>> GetMatches(string regexValue, string original)
        {
            var matches = new List<string>();

            if (IsNullOrEmpty(original)) return new Tuple<string, List<string>>(original, matches);
            var matchCollection = Regex.Matches(original, regexValue);

            matches.AddRange(from Match match in matchCollection select match.Value);

            return new Tuple<string, List<string>>(original, matches);
        }

        /**
         * Remove an Amount of Matches from an inputted Match Set
         *
         * @param matchData       The Match Data to remove from with the form of originalString:listOfMatches
         * @param parsedMatchData The Parsed Argument Data to match against, if available, to prevent Null Arguments
         * @param maxMatches      The maximum amount of matches to remove (Set to -1 to Remove All)
         * @return The original String from Match Data with the matches up to maxMatches removed
         */
        public static string RemoveMatches(Tuple<string, List<string>> matchData,
            List<Tuple<string, string>> parsedMatchData, int maxMatches)
        {
            var finalString = "";

            if (matchData == null) return finalString;
            finalString = matchData.Item1;
            var matchList = matchData.Item2;

            if (!matchList.Any()) return finalString;
            var foundMatches = 0;

            foreach (var match in matchList)
            {
                var isValidScan = foundMatches >= maxMatches;
                var alreadyRemoved = false;

                if (parsedMatchData != null && parsedMatchData.Any())
                    // Scan through Parsed Argument Data if Possible
                    if (parsedMatchData.Any(parsedArgument =>
                        parsedArgument.Item1.Equals(match, StringComparison.OrdinalIgnoreCase) &&
                        IsNullOrEmpty(parsedArgument.Item2)))
                    {
                        finalString = finalString.Replace(match, "");
                        alreadyRemoved = true;
                    }

                if (alreadyRemoved) continue;
                if (isValidScan) finalString = finalString.Replace(match, "");

                foundMatches++;
            }

            return finalString;
        }

        /**
         * Replaces Data in a String with Case-Insensitivity
         *
         * @param source          The original String to replace within
         * @param targetToReplace The value to replace on
         * @param replaceWith     The value to replace the target with
         * @return The completed and replaced String
         */
        public static string ReplaceAnyCase(string source, string targetToReplace, string replaceWith)
        {
            return !IsNullOrEmpty(source)
                ? Regex.Replace(source, targetToReplace, replaceWith, RegexOptions.IgnoreCase)
                : "";
        }

        /**
         * Replaces Data in a sequential order, following Case-Insensitivity
         *
         * @param source      The original String to replace within
         * @param replaceData The replacement list to follow with the form of: targetToReplace:replaceWithValue
         * @return The completed and replaced String
         */
        public static string SequentialReplaceAnyCase(string source, List<Tuple<string, string>> replaceData)
        {
            if (IsNullOrEmpty(source)) return "";
            var finalResult = source;

            if (replaceData.Any())
                finalResult = replaceData.Aggregate(finalResult,
                    (current, replacementData) =>
                        ReplaceAnyCase(current, replacementData.Item1, replacementData.Item2));

            return finalResult;
        }

        /**
         * Determines whether a String classifies as NULL or EMPTY
         *
         * @param entry The String to evaluate
         * @return {@code true} if Entry is classified as NULL or EMPTY
         */
        public static bool IsNullOrEmpty(string entry)
        {
            return entry == null || string.IsNullOrEmpty(entry) || entry.Equals("null");
        }

        /**
         * Determines whether the Object's String Interpretation classifies as a valid Boolean
         *
         * @param entry The Object to evaluate
         * @return {@code true} if Entry is classified as a valid Boolean
         */
        public static bool IsValidBoolean(object entry)
        {
            return entry != null && IsValidBoolean(entry.ToString());
        }

        /**
         * Determines whether the String classifies as a valid Boolean
         *
         * @param entry The String to evaluate
         * @return {@code true} if Entry is classified as a valid Boolean
         */
        public static bool IsValidBoolean(string entry)
        {
            return !IsNullOrEmpty(entry) && (entry.Equals("true", StringComparison.OrdinalIgnoreCase) ||
                                             entry.Equals("false", StringComparison.OrdinalIgnoreCase));
        }

        /**
         * Determines whether an inputted String classifies as a valid Color Code
         *
         * @param entry The String to evaluate
         * @return {@code true} if Entry is classified as a valid Color Code
         */
        public static bool IsValidColorCode(string entry)
        {
            return !IsNullOrEmpty(entry) && (entry.StartsWith("#") || entry.Length == 6 || entry.StartsWith("0x") ||
                                             GetValidInteger(entry).Item1);
        }

        /**
         * Determine whether an inputted Object classifies as a valid Integer
         *
         * @param entry The Object to evaluate
         * @return A Tuple with the format of isValid:parsedIntegerIfTrue
         */
        public static Tuple<bool, int> GetValidInteger(object entry)
        {
            return entry != null ? GetValidInteger(entry.ToString()) : new Tuple<bool, int>(false, 0);
        }

        /**
         * Determine whether an inputted String classifies as a valid Integer
         *
         * @param entry The String to evaluate
         * @return A Tuple with the format of isValid:parsedIntegerIfTrue
         */
        public static Tuple<bool, int> GetValidInteger(string entry)
        {
            var finalSet = new Tuple<bool, int>(false, 0);

            if (IsNullOrEmpty(entry)) return finalSet;
            try
            {
                finalSet = new Tuple<bool, int>(true, int.Parse(entry));
            }
            catch (Exception)
            {
                // If Error occurs, this is not a valid Integer
            }

            return finalSet;
        }

        /**
         * Determine whether an inputted Object classifies as a valid Long
         *
         * @param entry The Object to evaluate
         * @return A Tuple with the format of isValid:parsedLongIfTrue
         */
        public static Tuple<bool, long> GetValidLong(object entry)
        {
            return entry != null ? GetValidLong(entry.ToString()) : new Tuple<bool, long>(false, 0);
        }

        /**
         * Determine whether an inputted String classifies as a valid Long
         *
         * @param entry The String to evaluate
         * @return A Tuple with the format of isValid:parsedLongIfTrue
         */
        public static Tuple<bool, long> GetValidLong(string entry)
        {
            var finalSet = new Tuple<bool, long>(false, 0);

            if (IsNullOrEmpty(entry)) return finalSet;
            try
            {
                finalSet = new Tuple<bool, long>(true, long.Parse(entry));
            }
            catch (Exception)
            {
                // If Error occurs, this is not a valid Long
            }

            return finalSet;
        }

        /**
         * Formats an IP Address based on Input
         *
         * @param input      The original String to evaluate
         * @param returnPort Whether to return the port or the IP without the Port
         * @return Either the IP or the port on their own, depending on conditions
         */
        public static string FormatIp(string input, bool returnPort)
        {
            if (IsNullOrEmpty(input)) return !returnPort ? "127.0.0.1" : "7777";
            var formatted = input.Split(new[] {':'}, 2);
            return !returnPort
                ? ElementExists(formatted, 0) ? formatted[0].Trim() : "127.0.0.1"
                : ElementExists(formatted, 0)
                    ? formatted[1].Trim()
                    : "7777";
        }

        /**
         * Converts a String into a Valid and Acceptable Icon Format
         *
         * @param original The original String to evaluate
         * @return The converted and valid String, in an iconKey Format
         */
        public static string FormatPackIcon(string original)
        {
            var formattedKey = original;
            if (IsNullOrEmpty(formattedKey))
            {
                return formattedKey;
            }

            if (formattedKey.Contains("\\s")) formattedKey = formattedKey.Replace("\\s+", "");
            if (formattedKey.Contains("'")) formattedKey = formattedKey.Replace("'", "");
            if (formattedKey.Contains(".")) formattedKey = formattedKey.Replace("\\.", "_");
            /*if (BRACKET_PATTERN.matcher(formattedKey).find()) {
                    formattedKey = BRACKET_PATTERN.matcher(formattedKey).replaceAll("");
                }
                if (STRIP_COLOR_PATTERN.matcher(formattedKey).find()) {
                    formattedKey = STRIP_COLOR_PATTERN.matcher(formattedKey).replaceAll("");
                }*/
            return formattedKey.ToLower().Trim();
        }

        /**
         * Converts input into a Properly Readable String
         *
         * @param original The original String to format
         * @param avoid    Flag to ignore method if true
         * @return The formatted and evaluated String
         */
        public static string FormatWord(string original, bool avoid = false)
        {
            var formattedKey = original;
            if (IsNullOrEmpty(formattedKey) || avoid) return formattedKey;

            if (formattedKey.Contains("_")) formattedKey = formattedKey.Replace("_", " ");

            if (formattedKey.Contains("-")) formattedKey = formattedKey.Replace("-", " ");

            if (formattedKey.Contains(" ")) formattedKey = formattedKey.Replace("\\s+", " ");

            return RemoveRepeatWords(CapitalizeWord(formattedKey)).Trim();
        }

        /**
         * Removes Duplicated Words within an inputted String
         *
         * @param original The original String
         * @return The evaluated String without duplicate words
         */
        public static string RemoveRepeatWords(string original)
        {
            if (IsNullOrEmpty(original)) return original;

            var lastWord = "";
            var finalString = new StringBuilder();
            var wordList = original.Split(' ');

            foreach (var word in wordList)
            {
                if (!IsNullOrEmpty(lastWord) && word.Equals(lastWord)) continue;
                finalString.Append(word).Append(" ");
                lastWord = word;
            }

            return finalString.ToString().Trim();
        }

        /**
         * Capitalizes the words within a specified string
         *
         * @param str The String to capitalize
         * @return The capitalized output string
         */
        public static string CapitalizeWord(string str)
        {
            var s = new StringBuilder();

            // Declare a character of space
            // To identify that the next character is the starting
            // of a new word
            var charIndex = ' ';
            foreach (var currentChar in str)
            {
                // If previous character is space and current
                // character is not space then it shows that
                // current letter is the starting of the word
                if (charIndex == ' ' && currentChar != ' ')
                    s.Append(char.ToUpper(currentChar));
                else
                    s.Append(currentChar);

                charIndex = currentChar;
            }

            // Return the string with trimming
            return s.ToString().Trim();
        }

        /**
         * Convert a String into a List of Strings, split up by new lines
         *
         * @param original The original String
         * @return The converted, newline-split list from the original String
         */
        public static List<string> SplitTextByNewLine(string original)
        {
            if (IsNullOrEmpty(original)) return new List<string>();
            var formattedText = original;
            if (formattedText.Contains("\n")) formattedText = formattedText.Replace("\n", "&newline&");

            if (formattedText.Contains("\\n")) formattedText = formattedText.Replace("\\n", "&newline&");

            if (formattedText.Contains("\\\\n+")) formattedText = formattedText.Replace("\\\\n+", "&newline&");
            return new List<string>(formattedText.Split(new[] {"&newline&"}, StringSplitOptions.None));
        }

        /**
         * Display a Message to the Player, via the in-game Chat Hud
         *
         * @param sender  The Entity to Send to (Must be a Player)
         * @param message The Message to send and display in chat
         */
        public static void SendMessageToPlayer(Player sender, string message)
        {
            var lines = SplitTextByNewLine(message);
            if (lines == null) return;
            foreach (var line in lines) Main.NewText(line);
        }

        /**
         * Determines if the Specified index exists in the List with a non-null value
         *
         * @param data  The Array of Strings to check within
         * @param index The index to check
         * @return {@code true} if the index element exists in the list with a non-null value
         */
        public static bool ElementExists(IEnumerable<string> data, int index)
        {
            return ElementExists(new List<string>(data), index);
        }

        /**
         * Determines if the Specified index exists in the List with a non-null value
         *
         * @param data  The List of Strings to check within
         * @param index The index to check
         * @return {@code true} if the index element exists in the list with a non-null value
         */
        public static bool ElementExists(List<string> data, int index)
        {
            bool result;
            try
            {
                result = data.Count >= index && !IsNullOrEmpty(data[index]);
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

        /**
         * Strips Color and Formatting Codes from the inputted String
         *
         * @param input The original String to evaluate
         * @return The Stripped and evaluated String
         */
        public static string StripColors(string input)
        {
            return IsNullOrEmpty(input) ? input : Regex.Replace(input, "<.*?>", "");
        }
    }
}