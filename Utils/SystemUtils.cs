/*
 * MIT License
 *
 * Copyright (c) 2018 - 2020 CDAGaming (cstack2011@yahoo.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Reflection;
using Terraria.ModLoader;

namespace CraftPresence.Utils
{
    /**
     * System and General Use Utilities
     *
     * @author CDAGaming
     */
    public class SystemUtils
    {
        /**
         * If the {@link SystemUtils#OsArch} is 64-bit or x64
         */
        public readonly bool Is64Bit;

        /**
         * If the {@link SystemUtils#OsName} can be classified as MAC
         */
        public readonly bool IsMac;

        /**
         * If the {@link SystemUtils#OsName} can be classified as WINDOWS
         */
        public readonly bool IsWindows;

        /**
         * The Architecture of the User's System
         */
        public readonly string OsArch;

        /**
         * The Name of the User's Operating System
         */
        public readonly string OsName;

        /**
         * The {@link SystemUtils#BeginningTimestamp} as a DateTime
         */
        private DateTime _beginningDateTime;

        /**
         * The Beginning Unix Timestamp to count down from
         */
        private long _beginningTimestamp;

        /**
         * The elapsed time since the application started (In Seconds)
         */
        private long _elapsedTime;

        /**
         * Whether the timer is currently active
         */
        private bool _isTiming;

        /**
         * Whether the Callbacks related to the Mod have been refreshed
         * In this case, the RPC updates every 2 seconds with this check ensuring such
         */
        private bool _refreshedCallbacks;

        /**
         * The {@link SystemUtils#CurrentTimestamp} as a DateTime
         */
        public DateTime CurrentDateTime;

        /**
         * The Current Epoch Timestamp in Milliseconds
         */
        public long CurrentTimestamp;

        /**
         * If Loading has been completed, classified as after callbacks resync once
         */
        public bool HasLoaded;

        /**
         * If the {@link SystemUtils#OsName} can be classified as LINUX
         */
        public bool IsLinux;

        /**
         * The Current Time remaining on the Timer
         */
        public int Timer;

        /**
         * The Directory the Application is running in
         */
        public string UserDir;

        /**
         * Initialize Os and Timer Information
         */
        public SystemUtils()
        {
            try
            {
                OsName = Environment.OSVersion.Platform.ToString();
                OsArch = Assembly.GetExecutingAssembly().GetName().ProcessorArchitecture.ToString();
                UserDir = Assembly.GetExecutingAssembly().Location;

                IsWindows = OsName.StartsWith("Win") || OsName.StartsWith("Windows");
                IsMac = OsName.StartsWith("Mac");
                IsLinux = OsName.StartsWith("Unix") || OsName.StartsWith("Linux") || OsName.StartsWith("LINUX") ||
                          !IsMac && !IsWindows;

                CurrentDateTime = DateTime.UtcNow;
                CurrentTimestamp = CurrentDateTime.Ticks;
                _elapsedTime = 0;

                Is64Bit = Environment.Is64BitOperatingSystem;
            }
            catch (Exception ex)
            {
                ModUtils.Log.Error(ModUtils.Translator.Translate("craftpresence.logger.error.system"));
                ModUtils.Log.DebugError(ex.Message);
            }
        }

        /**
         * The Event to run on each client tick, if passed initialization events
         *
         * Comprises of Synchronizing Data, and Updating Time-Related Data as needed
         */
        public void OnTick()
        {
            _elapsedTime = DateTime.UtcNow.Second - CurrentDateTime.Second;

            if (Timer > 0)
            {
                if (!_isTiming)
                    StartTimer();
                else
                    CheckTimer();
            }

            // Every <passTime> Seconds, refresh Callbacks and load state status
            if (_elapsedTime % ModContent.GetInstance<AdvancedSettingsConfig>().RefreshRate == 0)
            {
                if (_refreshedCallbacks) return;
                if (!HasLoaded && !StringUtils.IsNullOrEmpty(CraftPresence.Client.Status) &&
                    CraftPresence.Client.Status.Equals("ready", StringComparison.OrdinalIgnoreCase)) HasLoaded = true;

                CraftPresence.Client.UpdatePresence(CraftPresence.Client.BuildRichPresence());
                CraftPresence.Client.IpcInstance.Invoke();
                _refreshedCallbacks = true;
            }
            else
            {
                _refreshedCallbacks = false;
            }
        }

        /**
         * Begins the Timer, counting down from {@link SystemUtils#BeginningTimestamp}
         */
        private void StartTimer()
        {
            _beginningDateTime = DateTime.UtcNow;
            _beginningTimestamp = _beginningDateTime.Ticks + Timer * 1000L;
            _isTiming = true;
        }

        /**
         * Determines the Remaining Time until 0, and Stops he Timer @ 0 remaining
         */
        private void CheckTimer()
        {
            if (Timer > 0)
            {
                var remainingTime = (_beginningTimestamp - DateTime.UtcNow.Ticks) / 1000L;
                Timer = (int) remainingTime;
            }
            else if (_isTiming)
            {
                _isTiming = false;
            }
        }
    }
}