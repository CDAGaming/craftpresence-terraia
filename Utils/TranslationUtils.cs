/*
 * MIT License
 *
 * Copyright (c) 2018 - 2020 CDAGaming (cstack2011@yahoo.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Text;
using Terraria.ModLoader;

namespace CraftPresence.Utils
{
    /**
     * Translation and Localization Utilities based on Language Code
     *
     * @author CDAGaming
     */
    public class TranslationUtils
    {
        /**
         * The Stored Mapping of Language Request History
         *
         * Format: languageId:doesExist
         */
        private readonly Dictionary<string, bool> _requestMap = new Dictionary<string, bool>();

        /**
         * The Charset Encoding to parse translations in
         */
        private string _encoding;

        /**
         * The Language Id to Locate and Retrieve Translations
         */
        private string _languageId = "en_US";

        /**
         * The Target Id to locate the Language File
         */
        private string _modId;

        /**
         * The Stored Mapping of Valid Translations
         *
         * Format: unlocalizedKey:localizedString
         */
        private Dictionary<string, string> _translationMap = new Dictionary<string, string>();

        /**
         * If using a .Json or .Lang Language File
         */
        private bool _usingJson;

        /**
         * Whether this Module has been initialized
         */
        public bool Initialized;

        /**
         * Whether the Translations are utilizing Unicode Characters
         */
        public bool IsUnicode;

        /**
         * Sets initial Data and Retrieves Valid Translations
         *
         * @param doUseJson Toggles whether to use .Json or .Lang, if present
         */
        public TranslationUtils(bool doUseJson = false) : this("", doUseJson)
        {
            // N/A
        }

        /**
         * Sets initial Data and Retrieves Valid Translations
         *
         * @param theModId Sets the Target Mod Id to locate Language Files
         * @param doUseJson Toggles whether to use .Json or .Lang, if present
         */
        public TranslationUtils(string theModId, bool doUseJson = false) : this(theModId, doUseJson, "UTF-8")
        {
            // N/A
        }

        /**
         * Sets initial Data and Retrieves Valid Translations
         *
         * @param theModId Sets the Target Mod Id to locate Language Files
         * @param doUseJson Toggles whether to use .Json or .Lang, if present
         * @param dataEncoding The Charset Encoding to parse Language Files
         */
        public TranslationUtils(string theModId, bool doUseJson, string dataEncoding = "UTF-8")
        {
            SetLanguage(ModContent.GetInstance<AccessibilitySettingsConfig>() != null
                ? ModContent.GetInstance<AccessibilitySettingsConfig>().LanguageId
                : _languageId);
            SetModId(theModId);
            SetUsingJson(doUseJson);
            SetEncoding(dataEncoding);
            if (ModContent.GetInstance<CraftPresence>() != null) Init(dataEncoding);
        }

        private void Init(string dataEncoding = "UTF-8")
        {
            GetTranslationMap(dataEncoding);
            CheckUnicode();

            Initialized = true;
        }

        /**
         * The Event to Run on each Client Tick, if passed initialization events
         *
         * Comprises of Synchronizing Data, and Updating Translation Data as needed
         */
        public void OnTick()
        {
            // Terraria-Exclusive: Ensure the Mod Instance is available before
            // attempting to load the Translation Map
            // Not doing so causes an error as the class is not yet loaded
            if (!Initialized)
            {
                if (ModContent.GetInstance<CraftPresence>() != null) Init(_encoding);
            }
            else
            {
                if (ModContent.GetInstance<AccessibilitySettingsConfig>() == null ||
                    _languageId.Equals(ModContent.GetInstance<AccessibilitySettingsConfig>().LanguageId) ||
                    _requestMap.ContainsKey(ModContent.GetInstance<AccessibilitySettingsConfig>().LanguageId) &&
                    !_requestMap[ModContent.GetInstance<AccessibilitySettingsConfig>().LanguageId]) return;
                SetLanguage(ModContent.GetInstance<AccessibilitySettingsConfig>().LanguageId);
                GetTranslationMap(_encoding);
                CheckUnicode();
            }
        }

        /**
         * Determines whether the translations contain Unicode Characters
         */
        private void CheckUnicode()
        {
            IsUnicode = false;
            var i = 0;
            var totalLength = 0;

            foreach (var currentString in _translationMap.Values)
            {
                var currentLength = currentString.Length;
                totalLength += currentLength;

                for (var index = 0; index < currentLength; ++index)
                    if (currentString[index] >= 256)
                        ++i;
            }

            var f = i / (float) totalLength;
            IsUnicode = f > 0.1D;
        }

        /**
         * Toggles whether to use .Lang or .Json Language Files
         *
         * @param doUseJson Toggles whether to use .Json or .Lang, if present
         */
        private void SetUsingJson(bool doUseJson)
        {
            _usingJson = doUseJson;
        }

        /**
         * Sets the Language Id to Retrieve Translations for, if present
         *
         * @param newLanguageId The Language Id (Default: en_US
         */
        private void SetLanguage(string newLanguageId = "en_US")
        {
            _languageId = newLanguageId;
        }

        /**
         * Sets the Charset Encoding to parse Translations in, if present
         *
         * @param newEncoding The Charset Encoding (Default: UTF-8)
         */
        private void SetEncoding(string newEncoding = "UTF-8")
        {
            _encoding = newEncoding;
        }

        /**
         * Sets the Mod Id to target when locating Language Files
         *
         * @param newModId The Mod Id to target
         */
        private void SetModId(string newModId = "")
        {
            _modId = newModId;
        }

        /**
         * Retrieves and Synchronizes a List of Translations from a Language File
         */
        private void GetTranslationMap(string dataEncoding = "UTF-8")
        {
            _translationMap = new Dictionary<string, string>();

            try
            {
                var inData = Encoding.GetEncoding(dataEncoding).GetString(
                    ModContent.GetInstance<CraftPresence>().GetFileBytes("Resources/Lang/" + _languageId.ToLower() +
                                                                         (_usingJson ? ".json" : ".lang")));

                if (!StringUtils.IsNullOrEmpty(inData))
                {
                    var fileLines = inData.Split(new[] {"\r\n", "\r", "\n"}, StringSplitOptions.None);

                    foreach (var line in fileLines)
                    {
                        var currentString = line.Trim();
                        if (currentString.StartsWith("#") || currentString.StartsWith("[{}]") ||
                            (_usingJson ? !currentString.Contains(":") : !currentString.Contains("="))) continue;
                        var splitTranslation = currentString.Split(new[] {_usingJson ? ':' : '='}, 2);
                        if (_usingJson)
                        {
                            var str1 = splitTranslation[0].Substring(1, splitTranslation[0].Length - 1)
                                .Replace("\\n", "\n").Replace("\\", "").Trim();
                            var str2 = splitTranslation[1].Substring(2, splitTranslation[1].Length - 2)
                                .Replace("\\n", "\n").Replace("\\", "").Trim();
                            _translationMap.Add(str1, str2);
                        }
                        else
                        {
                            _translationMap.Add(splitTranslation[0].Trim(), splitTranslation[1].Trim());
                        }
                    }
                }
                else
                {
                    ModUtils.Log.Error("Translations for " + _modId + " do not exist for " + _languageId);
                    _requestMap.Add(_languageId, false);
                    SetLanguage();
                }
            }
            catch (Exception ex)
            {
                ModUtils.Log.Error(
                    "An Exception has Occurred while Loading Translation Mappings, Things may not work well...");
                ModUtils.Log.DebugError(ex.Message);
            }
        }

        /**
         * Translates an Unlocalized String, based on the Translations retrieved
         *
         * @param stripColors    Whether to Remove Color and Formatting Codes
         * @param translationKey The unLocalized String to translate
         * @param parameters     Extra Formatting Arguments, if needed
         * @return The Localized Translated String
         */
        public string Translate(bool stripColors, string translationKey, params object[] parameters)
        {
            var hasError = false;
            var translatedString = translationKey;
            try
            {
                if (_translationMap.ContainsKey(translationKey))
                    translatedString = string.Format(_translationMap[translationKey], parameters);
                else
                    hasError = true;
            }
            catch (Exception ex)
            {
                ModUtils.Log.Error("Exception Parsing " + translationKey);
                ModUtils.Log.DebugError(ex.Message);
                hasError = true;
            }

            if (hasError) ModUtils.Log.Error("Unable to retrieve a Translation for " + translationKey);

            return stripColors ? StringUtils.StripColors(translatedString) : translatedString;
        }

        /**
         * Translates an Unlocalized String, based on the Translations retrieved
         *
         * @param translationKey The unLocalized String to translate
         * @param parameters     Extra Formatting Arguments, if needed
         * @return The Localized Translated String
         */
        public string Translate(string translationKey, params object[] parameters)
        {
            return Translate(
                ModContent.GetInstance<AccessibilitySettingsConfig>() != null &&
                ModContent.GetInstance<AccessibilitySettingsConfig>().StripTranslationColors, translationKey,
                parameters);
        }
    }
}